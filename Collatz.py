#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List


class CollatzSolver:
    """
    Objects of this class can be used to find Collatz cycle lengths. Each
    object has its own set of caches.
    """

    RANGE_LEN = 100

    def __init__(self):
        """
        Initialize a new object with fresh caches.
        """
        # Start with 1 already cached. This makes the logic in cycle_length
        # somewhat simpler.
        self.cache = {1: 1}
        self.range_cache = {}

    def max_range(self, start: int, end: int) -> int:
        """
        start the beginning of the range, inclusive
        end the end       of the range, inclusive
        return the max cycle length of the range [start, end]
        except if start > end
        """

        def max_range_uncached(start: int, end: int) -> int:
            """
            Helper function to iterate through a range and find the max.
            Range is inclusive: [start, end]
            If an "invalid" or "empty" range is given, defaults to 0.
            """
            return max((self.cycle_length(n) for n in range(start, end + 1)), default=0)

        assert start <= end

        # Find the first and last RANGE_LEN boundaries in [start, end]
        rounded_up = -(-start // CollatzSolver.RANGE_LEN) * CollatzSolver.RANGE_LEN
        rounded_down = (end // CollatzSolver.RANGE_LEN) * CollatzSolver.RANGE_LEN

        if rounded_up > end:
            # The first boundary is past the end, we cannot use the cache in
            # this case
            return max_range_uncached(start, end)

        # Because the above check failed, we now know that there is at least
        # one boundary in the range, so the rounded start and end values should
        # not have passed each other.
        assert rounded_up <= rounded_down

        # Find the max of the portion before the first cacheable region
        m = max_range_uncached(start, rounded_up - 1)

        # Loop over the cacheable regions (if any, we could have rounded_up ==
        # rounded_down, in which case this loop gets skipped.)
        for range_start in range(rounded_up, rounded_down, CollatzSolver.RANGE_LEN):
            if range_start not in self.range_cache:
                # the range is not already cached, we must compute it.
                self.range_cache[range_start] = max_range_uncached(
                    range_start, range_start + CollatzSolver.RANGE_LEN - 1
                )
            # take the max with the previous largest
            m = max(m, self.range_cache[range_start])

        # find the max of the portion after cacheable regions
        return max(m, max_range_uncached(rounded_down, end))

    def cycle_length(self, n: int) -> int:
        """
        n the starting number
        return the cycle length for that number
        """
        assert n >= 1

        # Stores the path traversed through the Collatz graph. We start at n.
        path = []

        curr = n
        while curr not in self.cache:
            path.append(curr)
            # Compute the next Collatz value
            curr = curr // 2 if curr % 2 == 0 else 3 * curr + 1

        # curr now stores the last cached value
        cached_length = self.cache[curr]
        path_length = len(path)
        # update the cache
        for i, vertex in enumerate(path):
            self.cache[vertex] = cached_length + path_length - i

        # we should now have the input in the cache, since it is the first
        # element of path
        assert n in self.cache
        length = self.cache[n]

        assert length >= 1
        return length


# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    solver = CollatzSolver()
    for s in r:
        i, j = collatz_read(s)
        v = solver.max_range(min(i, j), max(i, j))
        collatz_print(w, i, j, v)
