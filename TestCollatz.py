#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_print, collatz_solve, CollatzSolver

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ---
    # max_range
    # ----

    def test_max_range_1(self):
        v = CollatzSolver().max_range(1, 10)
        self.assertEqual(v, 20)

    def test_max_range_2(self):
        v = CollatzSolver().max_range(100, 200)
        self.assertEqual(v, 125)

    def test_max_range_3(self):
        v = CollatzSolver().max_range(201, 210)
        self.assertEqual(v, 89)

    def test_max_range_4(self):
        v = CollatzSolver().max_range(900, 1000)
        self.assertEqual(v, 174)

    # ---
    # cycle_length
    # ----

    def test_cycle_length(self):
        solver = CollatzSolver()
        v = solver.cycle_length(2)
        self.assertEqual(v, 2)

        # check that caching works
        v = solver.cycle_length(2)
        self.assertEqual(v, 2)

        v = solver.cycle_length(10)
        self.assertEqual(v, 7)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
