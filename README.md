# CS373: Software Engineering Collatz Repo

* Name: Robert Hrusecky

* EID: reh3279

* GitLab ID: robert-hrusecky

* HackerRank ID: robert_hrusecky

* Git SHA: 7e0aeafbc531da20e4802ae976f37e91a33c7678

* GitLab Pipelines: https://gitlab.com/robert-hrusecky/cs373-collatz/pipelines

* Estimated completion time: 8

* Actual completion time: 8

* Comments: None
